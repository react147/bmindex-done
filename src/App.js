import logo from './logo.svg';
import './App.css';

function App() {
  let c=0;
  const calc=()=>{
    const paino = document.getElementById("weight").value
    const pituus = document.getElementById("height").value
    const bmi = paino/(pituus*pituus)
    document.getElementById("id").innerHTML= "Your index is " + bmi.toFixed(1)
  }

  return (
    <div className="App">
      <p>Weigth (in kilos like 69)<input id="weight" type="number"></input></p>
      <p>Heigth (in meters like 1.65)<input id="height" type="number"></input></p>
      <button onClick={calc}>Laske</button>
      <div id="id"></div>
      <hr/>
      <img src="bmi.jpg" alt="BMI-table"></img>
    
    </div>
  );
}

export default App;
